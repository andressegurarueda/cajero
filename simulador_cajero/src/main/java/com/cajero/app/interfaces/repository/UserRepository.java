package com.cajero.app.interfaces.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cajero.app.dto.DtoUsuario;

@Repository
public interface UserRepository extends JpaRepository<DtoUsuario, String>{

}
