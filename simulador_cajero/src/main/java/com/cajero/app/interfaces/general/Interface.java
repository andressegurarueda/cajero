package com.cajero.app.interfaces.general;



public interface Interface <DAO, G> {
	

	public DAO read(int id);
	public boolean update(int id, G valor);
	public boolean save();

}
