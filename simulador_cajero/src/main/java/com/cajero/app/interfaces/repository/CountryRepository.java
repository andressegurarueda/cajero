package com.cajero.app.interfaces.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cajero.app.dto.DtoPais;

@Repository
public interface CountryRepository extends JpaRepository <DtoPais, String> {

}
