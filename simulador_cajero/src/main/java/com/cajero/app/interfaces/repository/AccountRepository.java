package com.cajero.app.interfaces.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cajero.app.dto.DtoCuenta;

@Repository
public interface AccountRepository extends JpaRepository <DtoCuenta, Integer>{
	
	

}
