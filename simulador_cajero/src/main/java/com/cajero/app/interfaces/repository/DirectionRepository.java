package com.cajero.app.interfaces.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cajero.app.dto.DtoDireccion;

@Repository
public interface DirectionRepository extends JpaRepository <DtoDireccion, String> {

}
