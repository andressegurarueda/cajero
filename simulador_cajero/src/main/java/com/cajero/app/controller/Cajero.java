package com.cajero.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cajero.app.dao.DaoCuenta;
import com.cajero.app.dao.DaoTarjeta;

@Controller
public class Cajero {
	
	@Autowired
	private DaoCuenta daoCuenta;
	@Autowired
	private DaoTarjeta daoTarjeta;
	
	
	@GetMapping("/balance")
	 public double getBalance(@RequestParam(name="numTarjeta", required = true)int numTarjeta) {
		DaoTarjeta tarjeta = daoTarjeta.read(numTarjeta);
		DaoCuenta cuenta = daoCuenta.read(tarjeta.numCuenta);
		System.out.println("Saldo de la cuenta: "+ cuenta.valor);
		return cuenta.valor;	 
	 }

	
	@GetMapping("/record")
	 public void updateBalance(@RequestParam(name="numTarjeta", required = true)int numTarjeta, 
			 @RequestParam(name="nuevoSaldo", required = true)double nuevoSaldo) {
		DaoTarjeta tarjeta = daoTarjeta.read(numTarjeta);
		DaoCuenta cuenta = daoCuenta.read(tarjeta.numCuenta);
		if(nuevoSaldo > 0) {
			daoCuenta.update(tarjeta.numCuenta,cuenta.valor + nuevoSaldo);
		}
		
				
	 }
	
	@GetMapping("/remove")
	 public void removeBalance(@RequestParam(name="numTarjeta", required = true)int numTarjeta, 
			 @RequestParam(name="retirar", required = true)double retirar) {
		DaoTarjeta tarjeta = daoTarjeta.read(numTarjeta);
		DaoCuenta cuenta = daoCuenta.read(tarjeta.numCuenta);
		if(retirar > 0 && cuenta.valor > retirar) {
			daoCuenta.update(tarjeta.numCuenta,cuenta.valor - retirar);
		}
	 
	
 }
}