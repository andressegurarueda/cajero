package com.cajero.app.dao;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.cajero.app.dto.DtoCuenta;
import com.cajero.app.interfaces.general.Interface;
import com.cajero.app.interfaces.repository.AccountRepository;


@Component
public class DaoCuenta implements Interface<DaoCuenta , Double>{
	
	
	public int numCuenta;
	public String tipoCuenta;
	public int idUsuario;
	public double valor;
	
	
	private DtoCuenta dtoCuenta;
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Override
	public boolean save() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public DaoCuenta read(int idCuenta) {
		Optional<DtoCuenta>  optionalAccount = accountRepository.findById(idCuenta);
		DaoCuenta daoAccount = new DaoCuenta();
		daoAccount.numCuenta = optionalAccount.get().getNumCuenta();
		daoAccount.tipoCuenta = optionalAccount.get().getTipo_cuenta();
		daoAccount.idUsuario = optionalAccount.get().getUsuario_idUsuario();
		daoAccount.valor = optionalAccount.get().getSaldo();
		return daoAccount;
	}

	@Override
	public boolean update(int idCuenta, Double nuevoValor) {
		
		try {
		Optional<DtoCuenta>  optionalAccount = accountRepository.findById(idCuenta);
		dtoCuenta = optionalAccount.get();
		dtoCuenta.setSaldo(nuevoValor);
		accountRepository.save(dtoCuenta);
		return true;
		
		}catch(Exception e){
			
		System.out.println("Error al actualizar saldo: "+ e.getMessage());	
		} 
		return false;
		
	}

}
