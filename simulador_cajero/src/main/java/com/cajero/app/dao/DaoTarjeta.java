package com.cajero.app.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cajero.app.dto.DtoTarjeta;
import com.cajero.app.interfaces.general.Interface;
import com.cajero.app.interfaces.repository.CardRepository;

@Component
public class DaoTarjeta implements Interface<DaoTarjeta, Integer>{

	public int numTarjeta;
	public int numCuenta;
	public int idUsuario;
	public double clave;
	
	
	private DtoTarjeta dtoTarjeta;
	@Autowired
	private CardRepository cardRepository;
	
	public DaoTarjeta() {
		dtoTarjeta = new DtoTarjeta();
	}
	
	@Override
	public boolean save() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public DaoTarjeta read(int idTarjeta) {
		Optional<DtoTarjeta> optionalCard = cardRepository.findById(idTarjeta);
		DaoTarjeta daoTarjeta = new DaoTarjeta();
		daoTarjeta.numTarjeta = optionalCard.get().getNum_tarjeta();
		daoTarjeta.numCuenta = optionalCard.get().getNumcuenta();
		daoTarjeta.idUsuario = optionalCard.get().getIdusuario();
		daoTarjeta.clave = optionalCard.get().getClave();
		return daoTarjeta;
	}

	
	@Override
	public boolean update(int idTarjeta, Integer valor) {
		// TODO Auto-generated method stub
		return false;
	}

	

	
	

	


}
