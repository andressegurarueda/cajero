package com.cajero.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimuladorCajeroApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimuladorCajeroApplication.class, args);
	}

}
