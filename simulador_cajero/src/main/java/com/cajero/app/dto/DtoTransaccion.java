package com.cajero.app.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Transaccion")
public class DtoTransaccion {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idtransaccion")
	public int idtransaccion;
	
	@Column(name="descripcion")
	public String descripcion;
	
	@Column(name="tipo_transaccion")
	public String tipo_transaccion;
	
	@Column(name="fecha")
	public Date fecha;
	
	@Column(name="Banco_idBanco")
	private int id_banco;
	
	@Column(name="Usuario_idUsuario")
	private int id_usuario;

	public int getId_transaccion() {
		return idtransaccion;
	}

	public void setId_transaccion(int id_transaccion) {
		this.idtransaccion = id_transaccion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTipo_transaccion() {
		return tipo_transaccion;
	}

	public void setTipo_transaccion(String tipo_transaccion) {
		this.tipo_transaccion = tipo_transaccion;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public int getId_banco() {
		return id_banco;
	}

	public void setId_banco(int id_banco) {
		this.id_banco = id_banco;
	}

	public int getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	
    
}
