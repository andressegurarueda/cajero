package com.cajero.app.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Cuenta")
public class DtoCuenta {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="num_cuenta")
	private Integer NumCuenta;
	
	@Column(name="tipo_cuenta")
	private String tipo_cuenta;
	
	@Column(name="saldo")
	private double saldo;
	
	@Column(name="usuario_idusuario")
	private int Usuario_idUsuario;

	@Column(name="banco_idbanco")
	private int id_banco;

	public int getNumCuenta() {
		return NumCuenta;
	}

	public void setNumCuenta(int numCuenta) {
		NumCuenta = numCuenta;
	}

	public String getTipo_cuenta() {
		return tipo_cuenta;
	}

	public void setTipo_cuenta(String tipo_cuenta) {
		this.tipo_cuenta = tipo_cuenta;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public int getUsuario_idUsuario() {
		return Usuario_idUsuario;
	}

	public void setUsuario_idUsuario(int usuario_idUsuario) {
		Usuario_idUsuario = usuario_idUsuario;
	}

	public int getId_banco() {
		return id_banco;
	}

	public void setId_banco(int id_banco) {
		this.id_banco = id_banco;
	}

	
	
}
