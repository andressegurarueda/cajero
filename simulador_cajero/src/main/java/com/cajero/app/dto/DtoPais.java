package com.cajero.app.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Pais")
public class DtoPais {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idpais;
	
	@Column(name="nombre")
	private String nombre;

	public int getdpais() {
		return idpais;
	}

	public void setId_pais(int id_pais) {
		this.idpais = id_pais;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
}
