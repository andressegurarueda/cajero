package com.cajero.app.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Banco")
public class DtoBanco {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idBanco")
	private int idbanco;
		
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="telefono")
	private int telefono;

	public int getIdbanco() {
		return idbanco;
	}

	public void setIdbanco(int idbanco) {
		this.idbanco = idbanco;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}
	
}
