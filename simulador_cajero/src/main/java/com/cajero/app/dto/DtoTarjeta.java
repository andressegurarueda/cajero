package com.cajero.app.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Tarjeta")
public class DtoTarjeta {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="num_tarjeta")
	private int num_tarjeta;
	
	@Column(name="num_cuenta")
	private  int num_cuenta;
	
	@Column(name="clave")
	private int clave;
	
	@Column(name="usuario_idusuario")
	private int idusuario;
	
	@Column(name="cuenta_num_cuenta")
	private int numcuenta;

	public int getNum_tarjeta() {
		return num_tarjeta;
	}

	public void setNum_tarjeta(int num_tarjeta) {
		this.num_tarjeta = num_tarjeta;
	}

	public int getNum_cuenta() {
		return num_cuenta;
	}

	public void setNum_cuenta(int num_cuenta) {
		this.num_cuenta = num_cuenta;
	}

	public int getClave() {
		return clave;
	}

	public void setClave(int clave) {
		this.clave = clave;
	}

	public int getIdusuario() {
		return idusuario;
	}

	public void setIdusuario(int idusuario) {
		this.idusuario = idusuario;
	}

	public int getNumcuenta() {
		return numcuenta;
	}

	public void setNumcuenta(int numcuenta) {
		this.numcuenta = numcuenta;
	}
	
	
	
	

	
}
